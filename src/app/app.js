var app = angular.module('yajimApp',  ['ui.router']);

app.factory('authInterceptor', function ($rootScope, $q, $window) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            if ($window.sessionStorage.token) {
                config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
            }
            return config;
        },
        response: function (response) {
            if (response.status === 401) {
                // handle the case where the user is not authenticated
            }
            return response || $q.when(response);
        }
    };
});

	app.config(['$httpProvider','$stateProvider','$urlRouterProvider','$locationProvider', function($httpProvider,$stateProvider, $urlRouterProvider, $locationProvider) {
        $httpProvider.interceptors.push('authInterceptor');
		$locationProvider.html5Mode(true);
		$urlRouterProvider.otherwise('/');

		$stateProvider
		.state('carousel', {
			url: '/',
			templateUrl: 'app/layout-blocks/layouts/carousel/carousel.html',
			controller: 'carouselCtrl'
		})
		.state('yajim', {
			url: '/yajim',
			views: {
				'' : { 
					templateUrl: 'app/layout-blocks/layouts/yajim/yajim.html',
					controller: 'yajimCtrl'
				},
				'header@yajim': {
					templateUrl: 'app/layout-blocks/l-header/header.html',
					controller: 'headerCtrl'
				},
				'leftColumn@yajim': {
					templateUrl: 'app/layout-blocks/l-left-col/left-col.html',
					controller: 'leftCtrl' 
				},
				'centerColumn@yajim': {
					templateUrl: 'app/layout-blocks/l-center-col/center-col.html',
					controller: 'centerCtrl' 
				},
				'rightColumn@yajim': {
					templateUrl: 'app/layout-blocks/l-right-col/right-col.html',
					controller: 'rightCtrl' 
				}
			},
		})
	}]);
app.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});


