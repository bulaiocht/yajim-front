app.controller('carouselCtrl', ['$scope', '$http','$state','$window', function($scope,$http,$state,$window){
    let loginBtn = document.getElementById('login-btn');
    let modal = document.getElementById('myModal');

    //Отрытия модального окна для регистрации

    loginBtn.onclick = function(event) {
        modal.style.display = 'block';
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    // Авторизация и проврка логина

    $scope.isAuthenticated = false;
    $scope.message = '';
    url = 'www.rest.yajim.tk';

    $scope.login_data={
        'username':'',
        'password':''
    };

    $scope.matchPattern = new RegExp(/[A-Za-z0-9_-]+/);

    $scope.requireValue = true;

    $scope.response={}

    $scope.loaded = false;
    $scope.CheckUser = function (userDetails) {
        $scope.message = userDetails.name
            + " (" + userDetails.pass + ")";
        console.log($scope.message)
    }
    //Логинизация и получение токена

    $scope.login = function (){
        var req = {
            method: 'POST',
            url: 'http://'+url+'/signin',
            headers: {
                'Content-Type': 'application/json'
            },
            data: $scope.login_data
        };
        $http(req).then(function successCallback(response, status, headers, config) {
            $window.sessionStorage.token = response.data.access_token;
            $scope.isAuthenticated = true;
            // console.log(sessionStorage.token);
            // console.log(response.data);
            // $scope.loaded = true;
            $state.go('yajim');
        }, function errorCallback(response,status, headers, config) {
            delete $window.sessionStorage.token;
            $scope.isAuthenticated = false;
            modal.style.display = 'block';
            $scope.message = 'Error: Invalid user or password';
        });

    };

}]);

