app.controller('headerCtrl', ['$scope','$window', function($scope,$window){
    $scope.headerCtrl = {
        title: 'Logo img here'
    };
    $scope.hideMenu = true;
	$scope.openMenu = function() {
		$scope.hideMenu = !$scope.hideMenu;
	}
    $scope.logout = function () {
        $scope.isAuthenticated = false;
        delete $window.sessionStorage.token;

    };
}]);