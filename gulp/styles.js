'use strict';

const gulp = require('gulp'),
 sass = require('gulp-sass'),
 watch = require('gulp-watch'),
 prefixer = require('gulp-autoprefixer'), 
 sourcemaps = require('gulp-sourcemaps'),
 cssmin = require('gulp-clean-css'),
 rename = require('gulp-rename'),
 urlAdjuster = require('gulp-css-url-adjuster'),
 concat = require('gulp-concat-css'),
 browserSync = require("browser-sync"),
 reload = browserSync.reload;


let path = {
    build: { 
        css: 'build/css/'
    },
    src: { 
        style: 'src/**/*.sass'
    },
    watch: { 
        style: 'src/**/*.+(scss|sass)'
    },
    clean: './build'
};    

gulp.task('style:build', function () {
    gulp.src(path.src.style) 
        .pipe(sourcemaps.init())
        .pipe(sass()) 
        .pipe(prefixer()) 
        .pipe(cssmin()) 
        .pipe(rename({dirname: ''}))
        .pipe(concat('main.css'))
        . pipe(urlAdjuster({
            replace:  ['../../app/layout-blocks/','/img/'],
          }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css)) 
        .pipe(reload({stream: true}));
});

gulp.task('watch', function(){
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });

});