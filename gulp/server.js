'use strict';

const gulp = require('gulp'),
 browserSync = require("browser-sync"),
 reload = browserSync.reload;



let config = {
    server: {
        baseDir: "./build"
    },
    tunnel: true,
    host: 'localhost',
    port: 3000,
    logPrefix: "YAJIM,YO!"
};


 gulp.task('webserver', function () {
    browserSync(config);
});
