'use strict';

const gulp = require('gulp'),
 sass = require('gulp-sass'),
 watch = require('gulp-watch'),
 prefixer = require('gulp-autoprefixer'), 
 uglify = require('gulp-uglify'),
 sourcemaps = require('gulp-sourcemaps'),
 rigger = require('gulp-rigger'),
 cssmin = require('gulp-clean-css'),
 imagemin = require('gulp-imagemin'),
 pngquant = require('imagemin-pngquant'),
 del = require('del'),
 useref = require('gulp-useref'),
 gulpif = require('gulp-if'),
 browserSync = require("browser-sync"),
 reload = browserSync.reload,
 concat = require('gulp-concat'),
 nodemon = require('gulp-nodemon')


let path = {
    build: { 
        html: 'build/',
        js: 'build/js/',
        img: 'build/img/',
        fonts: 'build/fonts/'
    },
    src: { 
        html: 'src/**/*.html',
        js: 'src/assets/js/main.js',
        img: 'src/assets/img/**/*.*', 
        fonts: 'src/assets/fonts/**/*.*'
    },
    watch: { 
        html: 'src/**/*.html',
        js: 'src/**/*.js',
        img: 'src/assets/img/**/*.*',
        fonts: 'src/assets/fonts/**/*.*'
    },
    clean: './build'
};

var jsPth = './src/**/*.js'; 



gulp.task('html:build', function () {
    gulp.src(path.src.html) 
        .pipe(useref())
        .pipe(gulpif('*.css', cssmin()))
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html)) 
        .pipe(reload({stream: true})); 
});

gulp.task('js:build', function () {
    gulp.src(jsPth) 
        // .pipe(rigger())
        .pipe(concat('main.js')) 
        .pipe(sourcemaps.init()) 
        // .pipe(uglify()) 
        .pipe(sourcemaps.write()) 
        .pipe(gulp.dest(path.build.js)) 
        .pipe(reload({stream: true})); 
});



gulp.task('image:build', function () {
    gulp.src(path.src.img) 
        .pipe(imagemin({ 
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img)) 
        .pipe(reload({stream: true}));
});


gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});


gulp.task('build', [
    'html:build',
    'js:build',
    'style:build',
    'fonts:build',
    'image:build'
]);


gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
});



gulp.task('clean', function() {
  return del.sync('build');
})

gulp.task('start', ['clean', 'build', 'webserver', 'watch']);
